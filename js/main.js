/*jslint browser: true */
/*global G_vmlCanvasManager */
let play,
    changeBG,
    save_note,
    login,
    notes_list,
    create_notes,
    iteration,
    select,
    opt,
    isNoteList,
    logout;
window.onload = function () {
  'use strict'

  const logged_user = document.getElementById("logged_user");
  logged_user.innerHTML = "Welcome " + localStorage.getItem("logged_user") + "!";

  //================NAVIGATION==================//
  login = function () {
    console.log("login")
    isNoteList = localStorage.setItem('isNoteList', false)
    window.location.href = "./index.html"
  }
  notes_list = function () {
    isNoteList = localStorage.setItem('isNoteList', true)
    console.log("notes_list")
    window.location.href = "./notes_list.html"
  }
  create_notes = function () {
    isNoteList = localStorage.setItem('isNoteList', false)
    console.log("create_notes")
    window.location.href = "./notes_creation_log.html"
  }
  logout = function () {
    alert("Thank you for Playing " + localStorage.getItem("logged_user") + "!")
    isNoteList = localStorage.setItem('isNoteList', false)
    window.location.href = "./index.html"
  }

  //=================Added feature=================//
  changeBG = function() {
    backgrounds = ['burlywood', 'blue', 'whitesmoke', 'beige', 'bisque']
    canvasBG = backgrounds[Math.floor(Math.random()*backgrounds.length)]
    canvas.style.backgroundColor = canvasBG;
    console.log(canvasBG);
  }

  //================Saving========================//
  save_note = function() { 
    if (pointdb.length >= 1){
      localStorage.setItem(`Saved_notes no. ${i}`, JSON.stringify(pointdb));
      localStorage.setItem("iteration", i);
      pointdb = []
      x1 = undefined
      y1 = undefined
      i ++
      ctx.clearRect(0, 0, canvas.width, canvas.height)
      alert("Note is saved! Thanks!")
    } else {
      alert("Please enter notes!")
    }
  }

  //==============VARIABLES==================//
  // let canvas,
  let ctx,
      x0, y0,
      x1, y1,
      x2, y2,
      windowHeight,
      windowWidth,
      canvasBG = 'burlywood',
      pointdb = [],
      backgrounds = [],
      i,

  canvas = document.getElementById('canvas');
  canvas.height = window.innerHeight - 100; //y-axis
  canvas.width = window.innerWidth; //x-axis
  canvas.style.backgroundColor = canvasBG;
  ctx = canvas.getContext('2d')
  //================NOTES LIST=======================//

  select = document.getElementById('saved_notes');
  iteration = localStorage.getItem("iteration");
  console.log(JSON.parse(localStorage.getItem('isNoteList')))
  if (JSON.parse(localStorage.getItem('isNoteList'))) {
    for (let x = 1; x <= iteration; x++){
      opt = document.createElement("option");
      opt.value = `Saved_notes no. ${x}`;
      opt.innerHTML = `Saved_notes no. ${x}`;
      select.appendChild(opt)
    }
    //Initial select drawing
    if (JSON.parse(localStorage.getItem(select.value)) != null) {
      pointdb = JSON.parse(localStorage.getItem(select.value))
      for (let p of pointdb) {
        console.log(p)
        drawLines(p)
      }
    }

    //If selection changes
    select.addEventListener('change', (event) => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.strokeStyle = 'black';
      pointdb = JSON.parse(localStorage.getItem(select.value))
      for (let p of pointdb) {
        console.log(p)
        drawLines(p)
      }
    });
  }

  //To count the number of saved notes
  if (localStorage.getItem('iteration') == null){
    i = 1
  }else {
    i = parseInt(localStorage.getItem('iteration')) + 1
    console.log(i)
  }

  function position(e){
    ctx.strokeStyle = 'black';
    if (x1 !== undefined && y1 !== undefined) {
      x2 = e.offsetX
      y2 = e.offsetY
      console.log("The x1 and y1: " + x1 + "," + y1)
      console.log("The x2 and y2: " + x2 + "," + y2)
      drawLine()
      console.log("The x1 and y1: " + x1 + "," + y1)
      console.log("The x2 and y2: " + x2 + "," + y2)
      console.log(pointdb)
      return
    }
    x1 = e.offsetX
    y1 = e.offsetY
    console.log(x1 + "," + y1)
    console.log(x2 + "," + y2)  
  }

  function drawLine(e){
    ctx.lineWidth = 5;
    ctx.lineCap = "round";

    ctx.beginPath();
    ctx.moveTo(x1, y1)
    ctx.lineTo(x2, y2);
    ctx.stroke();

    pointdb.push({'x1': x1, 'y1': y1,
                'x2': x2, 'y2': y2});
    x1 = x2
    y1 = y2
    x2 = undefined
    y2 = undefined
  }

  function drawLines(p) {
    ctx.lineWidth = 5;
    ctx.lineCap = "round";
    ctx.beginPath();
    ctx.moveTo(p.x1, p.y1);
    ctx.lineTo(p.x2, p.y2);
    ctx.stroke();
  }

  play = function() {
    //if the play button is triggered from note list
    if (JSON.parse(localStorage.getItem('isNoteList'))) {
      if (JSON.parse(localStorage.getItem(select.value)) != null) {
        pointdb = JSON.parse(localStorage.getItem(select.value))
      }else {
        alert("Nothing to play! Save a note first!")
        return
      } 
    }
    ctx.strokeStyle = 'red';
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    console.log('clearing')
    let x = 1000;
    console.log(pointdb);
    let timer = 100;
    for (let p of pointdb) {
      setTimeout(function(pObject) {
        console.log(pObject)
        drawLines(pObject);
      }, timer, p);
      timer += 100;
    }

  };

  //EventListeners
  canvas.addEventListener('mousedown', position);

//End of window.onload  
};


/*
=====WORKING SCRIBLE=========

function startPosition(e){
  isDown = true;
  console.log(isDown)
  draw(e)
}

function finishedPosition(e){
  isDown = false;
  console.log(isDown)
  ctx.beginPath();
}

function draw(e){
  if (!isDown) return; //if mouse is not pressed down, dont do anything
  ctx.lineWidth = 5;
  ctx.lineCap = "round";

  ctx.lineTo(e.offsetX, e.offsetY);
  ctx.stroke();

}

//EventListeners
canvas.addEventListener('mousedown', startPosition);
canvas.addEventListener('mouseup', finishedPosition);
canvas.addEventListener('mousemove', draw);

*/